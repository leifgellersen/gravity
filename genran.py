#! /usr/bin/env python3
"""
Example script for generating large-scale config file, because we're  helpful developers 😂
"""
import numpy as np

dots = np.random.rand(20,5)
dots[:,2:4] -= 0.5
dots[:,2:4] *= 0.01
(len(dots))
np.savetxt("test2.cfg",dots,header=str(len(dots)))
