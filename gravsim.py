"""
Module for simulating the motion of particles under Newtonian gravity 😍 💯

The user specifies initial particle position, momentum, mass and colour in a config file.
This is passed as a comand line argument to the gravsim executable. This ships with the default API.
"""

import io
import numpy as np
import copy
from scipy.spatial.distance import pdist, squareform

class FileParser:
    """Class for handling config files. 
    These can be written in either user-friendly or data-friendly format (and even a mix of the two 😮).
    See :ref:`configfiles`. 
    """

    def __init__(self):
        self.xmin=0.0
        self.xmax=1.0
        self.ymin=0.0
        self.ymax=1.0
        self.tscale=1
        self.color=[]
        return

    def parse_file(self, filename):
        """
        Args ➡:
            Config file

        ⬅ Returns:
            Array detailing particle positions, velocity, masses and colours 
        """
        fileobj = open(filename, "r")
        defonestar = [0.,0.,0.,0.,0.,"black"]
        onestar = copy.copy(defonestar)
        appflag = False
        allstars = []
        for line in fileobj:
            line=line.rstrip("\n").lstrip(" ")
            if line.startswith("#"):
                value=line.split("=")[-1]
                line=line.replace(' ', '')
                if line.startswith("#number"):
                    self.number = int(value)
                elif line.startswith("#xmin"):
                    self.xmin = float(value)
                elif line.startswith("#xmax"):
                    self.xmax = float(value)
                elif line.startswith("#ymin"):
                    self.ymin = float(value)
                elif line.startswith("#ymax"):
                    self.ymax = float(value)
                elif line.startswith("#tscale"):
                    self.tscale = float(value)
            elif line.find("=") != -1:
                appflag = True
                lsp = line.split("=")
                if lsp[0].startswith("x"):
                    onestar[0] = float(lsp[1])
                elif lsp[0].startswith("y"):
                    onestar[1] = float(lsp[1])
                elif lsp[0].startswith("vx"):
                    onestar[2] = float(lsp[1])
                elif lsp[0].startswith("vy"):
                    onestar[3] = float(lsp[1])
                elif lsp[0].startswith("m"):
                    onestar[4] = float(lsp[1])
                elif lsp[0].startswith("c"):
                    onestar[5] = str(lsp[1])
            elif not line.isspace():
                if appflag:
                    self.color.append(onestar[5])
                    onestar.pop()
                    allstars.append(onestar)
                    onestar=copy.copy(defonestar)
                    appflag = False
                if not line.startswith("["):
                    lsp = line.split(" ")
                    onestar = [float(lsp[0]),float(lsp[1]),float(lsp[2]),float(lsp[3]),float(lsp[4])]
                    if len(lsp) > 5:
                        self.color.append(lsp[5])
                    else:
                        self.color.append("black")
                    allstars.append(onestar)
                    onestar=copy.copy(defonestar)
        if appflag:
            print(onestar)
            self.color.append(onestar[5])
            onestar.pop()
            allstars.append(onestar)
            onestar=copy.copy(defonestar)
            appflag = False
        data=np.asarray(allstars, dtype=np.float64)
        print(data)
        return data

class Solver:
    """
    Class where all the physics happens 🤓

    Args ➡:
        * y0 (array): 4xN-D array of the form: x_i, y_i, vx_i, vy_i \\
        * m (array): 1D array of particle masses
        * tscale: Used to set speed of simulation ⌛
    """

    def __init__(self,y0,m,tscale=1):
        self.y0 = y0
        self.m = m
        self.dt = 0.001*tscale
        self.gc = 0.0001

    def next(self):
        """
        Adjust the position and velocities in y0 according to gravity. This is done over a timestep of length set by tscale.
        """
        for _ in range(100):
            self._step()
        #print(self.y0)
        return self.y0
        # Gravity
        #self.y0[:,3] -= 5e-5
        # Collisions
        #collisions = np.where(squareform(pdist(self.y0[:,:2]) < 0.005))
        #dots[collisions,2:] = 0
        #self.y0[collisions,2:] *= 0
        #touching_floor = (self.y0[:,1] < 0.0) | (self.y0[:,1] > 1)
        #touching_wall = (self.y0[:,0] < 0.0) | (self.y0[:,0] > 1)
        #self.y0[ touching_floor , 3  ] *= -1.0
        #self.y0[ touching_wall , 2  ] *= -1.0
        #return self.y0
    def _step(self):
        """
        
        """
        distances = pdist(self.y0[:,0:2])
        dists = squareform(distances)
        for i in range(self.y0.shape[0]):
           # for j in range(self.y0.shape[0]):
            #    if i == j:
             #       continue
              #  dist = np.sqrt((self.y0[i][0]-self.y0[j][0])**2+(self.y0[i][1]-self.y0[j][1])**2)
            xforce = np.sum((self.gc*self.m[j]/dists[i,j]**3)*(self.y0[j][0]-self.y0[i][0]) for j in range(self.y0.shape[0]) if j!=i)
            yforce = np.sum((self.gc*self.m[j]/dists[i,j]**3)*(self.y0[j][1]-self.y0[i][1]) for j in range(self.y0.shape[0]) if j!=i)
                # 1st particle, x and y velocities
               # self.y0[i][2] += dt*gc*self.m[j]/dist**3*(self.y0[j][0]-self.y0[i][0])
                #self.y0[i][3] += dt*gc*self.m[j]/dist**3*(self.y0[j][1]-self.y0[i][1])
            self.y0[i][2] +=self.dt*xforce
            self.y0[i][3] +=self.dt*yforce
        self.y0[:,:2] += self.dt*self.y0[:,2:]
