.. _configfiles:

Config files
============

There are two syntaxes one can use to write a config file, as detailed below.
Note there is also an example script for generating a high multiplicity config file called genran.py

User-friendly
-------------
Configure each particle in the following way:

.. code-block:: python

   [name]
   m=...
   x=...
   y=...
   vx=...
   vy=...
   c=..


Data-friendly
-------------

Write file as array with each column of the form:

x_i y_i vx_i vy_i m_i colour_i

Keywords
--------

+---------+---------------------+
| #xmin   | Lower x-axis value  |
+---------+---------------------+
| #xmax   | Upper x-axis value  |
+---------+---------------------+
| ...     | ...                 |
+---------+---------------------+