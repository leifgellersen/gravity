.. Gravity Simulator documentation master file, created by
   sphinx-quickstart on Thu Sep  6 18:12:49 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Gravity Simulator 
=================

Welcome to the documentation for Gravity Simulator, a python module for graphical simulation of particle motion under Newtonian gravity.

Shout out to MCnet and the Goettingen mandem.

.. math::
	\frac{1}{2}R_{\mu\nu}


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   foo
   bar

.. autoclass 

:ref:`modindex`

:ref:`configfiles`
